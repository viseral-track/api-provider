package com.track.serviceprovider.controller;

import com.track.serviceprovider.datamodel.dto.DataRequestDTO;
import com.track.serviceprovider.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/record")
@Slf4j
public class APIController {


    @Autowired
    private RedisService redisService;

    @GetMapping("/type/{type}")
    public Mono<ResponseEntity> getTypeRecords(@PathVariable String type){
        if (type.equalsIgnoreCase("all")){
            return Mono.just(ok("All!"));
        }
        else {
            return redisService.findRecordsByType(type)
                    .flatMap(s -> Mono.just(ok(s)));
        }
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity> getSingleRecord(@PathVariable String id){
        return redisService.findSingleRecord(id)
                .flatMap(s -> Mono.just(ok(s)));
    }

    @PostMapping("/create")
    public Mono<ResponseEntity> saveRecord(@RequestBody DataRequestDTO data){
        return redisService.saveRecord(data)
                .flatMap(s -> Mono.just(ok(s)));
    }

    @GetMapping("/purchase/{user}")
    public Mono<ResponseEntity> getUserPurchases(@PathVariable String user){
        return redisService.findPurchases(user)
                .flatMap(s -> Mono.just(ok(s)));
    }

    @GetMapping("/list/{user}")
    public Mono<ResponseEntity> getListedItems(@PathVariable String user){
        log.info(user);
        return redisService.findListedItems(user)
                .flatMap(s -> Mono.just(ok(s)));
    }

}
