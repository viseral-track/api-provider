package com.track.serviceprovider.datamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataRecord {

    private String id;
    private String actionType;
    private String recordType;
    private String itemStatus;
    private String itemName;
    private String buyerName;
    private String sellerName;
    private String timestamp;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
