package com.track.serviceprovider.datamodel.dto;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataRequestDTO {

    private String actionType;
    private String recordType;
    private String itemName;
    private String sellerName;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
