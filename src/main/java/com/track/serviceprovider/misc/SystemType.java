package com.track.serviceprovider.misc;

import lombok.Getter;

public class SystemType {

    @Getter
    public static enum ActionType {

        BUY("Buy"),
        SELL("Sell"),
        HOLD("Hold"),
        REMOVE("Remove");

        private String name;
        ActionType(String name){
            this.name = name;
        }
    }

    @Getter
    public static enum ItemStatus {
        AVAILABLE("Available"),
        NOTAVAILABLE("Not Available"),
        UNLISTED("Unlisted");

        private String name;

        ItemStatus(String name){
            this.name = name;
        }
    }






}
