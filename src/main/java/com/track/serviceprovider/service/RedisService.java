package com.track.serviceprovider.service;

import com.track.serviceprovider.datamodel.DataRecord;
import com.track.serviceprovider.datamodel.dto.DataRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.ReactiveListOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveValueOperations;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import com.track.serviceprovider.misc.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@Slf4j
public class RedisService {

    @Autowired
    private ReactiveRedisOperations<String, DataRecord> ops;


    public Mono<List<DataRecord>> findRecordsByType(String type){
        return ops.keys("*")
                .flatMap(ops.opsForValue()::get)
                .filter(s -> s.getRecordType().equalsIgnoreCase(type))
                .collectList();
    }

    public Mono<List<DataRecord>> findAllRecords(){
        return ops.keys("*")
                .flatMap(ops.opsForValue()::get)
                .collectList();
    }

    public Mono<DataRecord> findSingleRecord(String id){
        return ops.keys("record-"+id)
                .flatMap(ops.opsForValue()::get)
                .next();
    }

    public Mono<DataRecord> findBestSingleRecordMatch(String itemName){
        return ops.keys("*")
                .flatMap(ops.opsForValue()::get)
                .filter(s -> s.getItemName().equalsIgnoreCase(itemName))
                .next();
    }

    public Mono<DataRecord> saveRecord(DataRequestDTO record){
        String uuid = UUID.randomUUID().toString();
        DataRecord permRecord = new DataRecord(uuid, record.getActionType(), record.getRecordType(), SystemType.ItemStatus.AVAILABLE.name(),
                record.getItemName(), "", record.getSellerName(), new Date().toString());
        return ops.opsForValue().set("record-"+uuid, permRecord)
                    .flatMap(s -> ops.opsForValue().get("record-"+uuid));
    }

    public Mono<List<DataRecord>> findPurchases(String buyerName){
        return ops.keys("*")
                .flatMap(ops.opsForValue()::get)
                .filter(s -> s.getBuyerName().equalsIgnoreCase(buyerName))
                .collectList();
    }

    public Mono<List<DataRecord>> findListedItems(String sellerName){
        return ops.keys("*")
                .flatMap(ops.opsForValue()::get)
                .filter(s -> s.getSellerName().equalsIgnoreCase(sellerName))
                .collectList();
    }

}
