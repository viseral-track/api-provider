package com.track.serviceprovider.config;

import com.track.serviceprovider.datamodel.DataRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveListOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveValueOperations;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@Slf4j
public class RedisConfig {

    @Bean
    @Primary
    ReactiveRedisOperations<String, DataRecord> redisOperations(ReactiveRedisConnectionFactory factory){
        Jackson2JsonRedisSerializer<DataRecord> serializer = new Jackson2JsonRedisSerializer<>(DataRecord.class);

        RedisSerializationContext.RedisSerializationContextBuilder<String, DataRecord> builder =
                RedisSerializationContext.newSerializationContext(new StringRedisSerializer());

        RedisSerializationContext<String, DataRecord> context = builder.value(serializer).build();

        return new ReactiveRedisTemplate<>(factory, context);
    }

    @Bean
    public ReactiveValueOperations<String, DataRecord> redisListOps(ReactiveRedisOperations<String, DataRecord> redisOps){
        return redisOps.opsForValue();
    }

}
